import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api
from flask_migrate import Migrate, migrate
from models.profile import ProfileModel
#from resources.profile_resources import ProfileResource
#from models.dog_model import Dog
#from resources.dog import DogResource

from db import db

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = \
    'sqlite:///' + os.path.join(basedir, 'database.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)
db.init_app(app)
migrate = Migrate(app, db)

#api.add_resource(ProfileResource, '/profile')
#api.add_resource(ProfilesResource, '/profiles')
#api.add_resource(DogResource, '/dog/<name>')
#api.add_resource(DogsResource, '/dogs')
#api.add_resource(OwnerDogsList, '/dogs_of/<string:last_name>')

if __name__ =='__main__':
    app.run(host="0.0.0.0", port=5002)

